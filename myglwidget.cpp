#include "myglwidget.h"
#include <iostream>
#include <stack>
#include "modelloader.h"

MyGLWidget::MyGLWidget()
{
  setFocusPolicy(Qt::StrongFocus);
}

MyGLWidget::MyGLWidget(QWidget*& w) : QOpenGLWidget(w), vbo(QOpenGLBuffer::VertexBuffer), ibo(QOpenGLBuffer::IndexBuffer)
{
  setFocusPolicy(Qt::StrongFocus);
}

void MyGLWidget::createQuad()
{
  // 1. Vertex Position
  this->vertices[0] = 1.0f;
  this->vertices[1] = -1.0f;
  this->vertices[2] = 0.0f;
  this->vertices[3] = 1.0f;
  // 1. Vertex Farbe
  this->vertices[4] = 1.0f;
  this->vertices[5] = 0.0f;
  this->vertices[6] = 0.0f;
  this->vertices[7] = 1.0f;

  // 2. Vertex Position
  this->vertices[8] = 1.0f;
  this->vertices[9] = 1.0f;
  this->vertices[10] = 0.0f;
  this->vertices[11] = 1.0f;
  // 2. Vertex Farbe
  this->vertices[12] = 0.0f;
  this->vertices[13] = 1.0f;
  this->vertices[14] = 0.0f;
  this->vertices[15] = 1.0f;

  // 3. Vertex Position
  this->vertices[16] = -1.0f;
  this->vertices[17] = 1.0f;
  this->vertices[18] = 0.0f;
  this->vertices[19] = 1.0f;
  // 3. Vertex Farbe
  this->vertices[20] = 0.0f;
  this->vertices[21] = 0.0f;
  this->vertices[22] = 1.0f;
  this->vertices[23] = 1.0f;

  // 4. Vertex Position
  this->vertices[24] = -1.0f;
  this->vertices[25] = -1.0f;
  this->vertices[26] = 0.0f;
  this->vertices[27] = 1.0f;
  // 4. Vertex Farbe
  this->vertices[28] = 1.0f;
  this->vertices[29] = 1.0f;
  this->vertices[30] = 1.0f;
  this->vertices[31] = 1.0f;

  //Initialisiere Elemente
  this->indices[0] = 0;
  this->indices[1] = 1;
  this->indices[2] = 3;
  this->indices[3] = 3;
  this->indices[4] = 1;
  this->indices[5] = 2;
}

void MyGLWidget::fillBuffer()
{
  // *** Initialisiere VBO ***
  // Lade Modell aus Datei
  // Anm.: Linux/Unix kommt mit einem relativen Pfad hier evtl. nicht zurecht
  ModelLoader model;
  bool res = model.loadObjectFromFile("/home/geoffrey/Documents/FH_Aachen/CG/INF_GC_Praktikum_3/P3_models/sphere_high.obj");
  // Wenn erfolgreich, generiere VBO und Index-Array
  if (res) {
    // Frage zu erwartende Array-Längen ab
    vboLength = model.lengthOfSimpleVBO();
    iboLength = model.lengthOfIndexArray();
    // Generiere VBO und Index-Array
    vboData = new GLfloat[vboLength];
    indexData = new GLuint[iboLength];
    model.genSimpleVBO(vboData);
    model.genIndexArray(indexData);
  }
  else {
    /*//Erzeuge VBOBuffer
    this->vbo.create();
    this->vbo.bind();
    this->vbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
    this->vbo.allocate(this->vertices, sizeof(GLfloat)*4*(4+4));
    this->vbo.release();
    //Erzeuge Index Buffer
    this->ibo.create();
    this->ibo.bind();
    this->ibo.setUsagePattern(QOpenGLBuffer::StaticDraw);
    this->ibo.allocate(this->indices,sizeof(GLubyte)*6);
    this->ibo.release();*/
      std::cout << "model nicht geladen..." << std::endl;
  }
  vbo.create();
  vbo.bind();
  vbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
  //vbo.allocate(vertices, sizeof(GLfloat) * 4 * 8);
  vbo.allocate(vboData, sizeof(GLfloat) * vboLength);
  vbo.release();

  ibo.create();
  ibo.bind();
  ibo.setUsagePattern(QOpenGLBuffer::StaticDraw);
  //ibo.allocate(indices, sizeof(GLubyte) * 36);
  ibo.allocate(indexData, sizeof(GLuint) * iboLength);
  ibo.release();
}

void MyGLWidget::initializeGL()
{
  this->createQuad();
  this->fillBuffer();

  // Lade die Shader-Sourcen aus externen Dateien (ggf. anpassen)
  this->shaderProgram.addShaderFromSourceFile(QOpenGLShader::Vertex,":/default130.vert");
  this->shaderProgram.addShaderFromSourceFile(QOpenGLShader::Fragment,":/default130.frag");
  // Kompiliere und linke die Shader-Programme
  this->shaderProgram.link();


  glEnable(GL_DEPTH_TEST);
  glCullFace(GL_BACK);
  glEnable(GL_CULL_FACE);//Disabled by default
  glDepthFunc(GL_LEQUAL);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

  glClearDepth(1.0f);
  glClearColor(.0f, .0f, .0f, .0f);//Set bgColor black

  // Lade die Shader-Sourcen aus externen Dateien (ggf. anpassen)
  shaderProgram.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/default130.vert");
  shaderProgram.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/default130.frag");
  // Kompiliere und linke die Shader-Programme
  shaderProgram.link();
}

void MyGLWidget::resizeGL(int width, int height)
{
  // Compute aspect ratio
  height = (height == 0) ? 1 : height;
  //GLfloat aspect = (GLfloat)width / (GLfloat)height;

  // Set viewport to cover the whole window
  glViewport(0, 0, width, height);
}

void MyGLWidget::paintGL()
{


  // Clear buffer to set color and alpha
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
/*
  // Apply model view transformations

  std :: stack<QMatrix4x4> matrixStack;
  // Der Einfachheit halber behandeln wir Perspective-
  // und Modelview-Matrix hier als eine Matrix
  QMatrix4x4 matrix;
  matrix.setToIdentity();
  matrix.perspective(60.0, 4.0/3.0, 0.1, 100.0) ;
  matrix.translate(0.0f + this->posX, 0.0f + this->posY, 0.0f - 7) ;
  matrix.rotate(45 + this->rotation + this->counter,1,1,1) ;
  matrix.scale(1 + this->posZ, 1 + this->posZ, 1 + this->posZ);
  matrixStack.push(matrix); // glPushMatrix
  matrix.translate(0.0, 0.0, 1.0) ;
  matrix = matrixStack.top(); // glPopMatrix
  matrixStack.pop(); // "


  this->shaderProgram.bind();
  this->vbo.bind();
  this->ibo.bind();


  // Lokalisiere bzw. definiere die Schnittstelle für die Eckpunkte
  int attrVertices = 0;
  attrVertices = this->shaderProgram.attributeLocation("vert"); // #version 130
  // Lokalisiere bzw. definiere die Schnittstelle für die Farben
  int attrColors = 1;
  attrColors = this->shaderProgram.attributeLocation("color"); // #version 130
  // Aktiviere die Verwendung der Attribute-Arrays
  this->shaderProgram.enableAttributeArray(attrVertices);
  this->shaderProgram.enableAttributeArray(attrColors);
  // Lokalisiere bzw. definiere die Schnittstelle für die Transformationsmatrix
  // Die Matrix kann direkt übergeben werden, da setUniformValue für diesen Typ
  // überladen ist
  int unifMatrix = 0;
  unifMatrix = this->shaderProgram.uniformLocation("matrix"); // #version 130
  this->shaderProgram.setUniformValue(unifMatrix, matrix);
  // Fülle die Attribute-Buffer mit den korrekten Daten
  int offset = 0;
  int stride = 8 * sizeof(GLfloat);
  this->shaderProgram.setAttributeBuffer(attrVertices, GL_FLOAT, offset, 4, stride);
  offset += 4 * sizeof(GLfloat);
  this->shaderProgram.setAttributeBuffer(attrColors, GL_FLOAT, offset, 4, stride);


  // Zeichne die 6 Elemente (Indizes) als Dreiecke
  // Die anderen Parameter verhalten sich wie oben
  glDrawElements(GL_TRIANGLES, this->iboLength, GL_UNSIGNED_INT, 0);

  this->shaderProgram.disableAttributeArray(attrVertices);
  this->shaderProgram.disableAttributeArray(attrColors);

  this->vbo.release();
  this->ibo.release();

  // Löse das Shader-Programm
  this->shaderProgram.release();

  // Increment this->counter
  this->counter++;
  this->update();
}

void MyGLWidget::receiveRotationZ(int value)
{
  this->rotation = value;
}

void MyGLWidget::keyPressEvent(QKeyEvent *event)
{
  switch(event->key())
  {
    case Qt::Key_Left:
    case Qt::Key_A:
      std::cout << "event left" << std::endl;
      this->posX-=.1;
      break;
    case Qt::Key_Up:
    case Qt::Key_W:
      std::cout << "event up" << std::endl;
      this->posY+=.1;
      break;
    case Qt::Key_Right:
    case Qt::Key_D:
      std::cout << "event right" << std::endl;
      this->posX+=.1;
      break;
    case Qt::Key_Down:
    case Qt::Key_S:
      std::cout << "event down" << std::endl;
      this->posY-=.1;
      break;
    default:
      QOpenGLWidget::keyPressEvent(event);
      break;
  }
}

void MyGLWidget::wheelEvent(QWheelEvent *event)
{
  if(event->delta() < 0 && this->posZ > -0.99){
    this->posZ-=0.01;
  }
  if(event->delta() > 0){
    this->posZ+=0.01;
  }
  std::cout << "mousewheel: " << event->delta() << std::endl
            << "pos z: " << this->posZ << std::endl;
  emit signalZoomZ(this->posZ+1);
}
#ifndef MYGLWIDGET_H
#define MYGLWIDGET_H

#include <QWidget>
#include <QOpenGLWidget>
#include <QKeyEvent>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>

class MyGLWidget : public QOpenGLWidget
{
  Q_OBJECT

public:
  MyGLWidget();
  MyGLWidget(QWidget*& w);

  // QGLWidget interface
public slots:
  void receiveRotationZ(int value);

  // QGLWidget interface
protected:
  void initializeGL();
  void resizeGL(int w, int h);
  void paintGL();
  void keyPressEvent(QKeyEvent *event);
  void wheelEvent(QWheelEvent *event);
  void createQuad();
  void fillBuffer();

private:
  unsigned int counter = 0;
  unsigned int rotation = 0;
  double posX = 0.0;
  double posY = 0.0;
  double posZ = 0.0;
  QOpenGLBuffer vbo;
  QOpenGLBuffer ibo;
  GLfloat vertices[4*(4+4)];
  GLubyte indices[6];
  QOpenGLShaderProgram shaderProgram;


  // Als Pointer, da die Größer später dynamisch festgelegt wird
  GLfloat* vboData;
  GLuint* indexData; // GLuint statt GLubyte, da viele Modelle groß sind
  unsigned int vboLength;
  unsigned int iboLength;



signals:
  void signalZoomZ(double);
};

#endif // MYGLWIDGET_H
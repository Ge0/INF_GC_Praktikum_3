#version 130
// default130.vert: a simple vertex shader

uniform mat4 matrix;
attribute vec4 vert;
attribute vec4 color;
varying vec4 col;

void main()
{
    col = vec4(1.0f,0.0f, 0.0f, 1.0f);
    //col = color;
    gl_Position = matrix * vert;
}